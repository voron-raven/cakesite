from cake.models import Category
import os


def categories(request):
    """Get all categories."""
    categories = Category.objects.all().order_by("name")
    return {'categories': categories}


def select_parent_template(request):
    """Check if it's ajax, if so no need to parent template."""
    parent_template = "dummy_parent.html" if request.is_ajax() else "base.html"
    return {'parent_template': parent_template}


def openshift(request):
    """Check if it's openshift."""
    if 'OPENSHIFT_APP_NAME' in os.environ:
        return {'OPENSHIFT': True}
    else:
        return {'OPENSHIFT': False}
