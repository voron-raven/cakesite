# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cake', '0004_auto_20150526_0111'),
    ]

    operations = [
        migrations.DeleteModel(
            name='Variable',
        ),
    ]
