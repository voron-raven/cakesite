# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cake', '0007_auto_20150526_2252'),
    ]

    operations = [
        migrations.AddField(
            model_name='cake',
            name='slug',
            field=models.SlugField(default=1, editable=False),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='category',
            name='slug',
            field=models.SlugField(default=2, editable=False),
            preserve_default=False,
        ),
    ]
