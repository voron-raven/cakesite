# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cake', '0008_auto_20150527_2213'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cake',
            name='slug',
            field=models.SlugField(),
        ),
        migrations.AlterField(
            model_name='category',
            name='slug',
            field=models.SlugField(),
        ),
    ]
