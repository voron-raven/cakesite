# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cake', '0005_delete_variable'),
    ]

    operations = [
        migrations.AddField(
            model_name='cake',
            name='picture',
            field=models.ImageField(default=b'/static/no-avatar.jpg', upload_to=b'cakes'),
        ),
    ]
