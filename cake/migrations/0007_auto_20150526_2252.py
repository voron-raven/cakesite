# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cake', '0006_cake_picture'),
    ]

    operations = [
        migrations.RenameField(
            model_name='cake',
            old_name='picture',
            new_name='image',
        ),
    ]
