from django.contrib import admin
from django.db import models
from django import forms
from cake.models import Category, Cake


class CategoryAdmin(admin.ModelAdmin):
    formfield_overrides = {models.TextField: {'widget': forms.Textarea(attrs={'class':'ckeditor'})},}

    class Media:
        js = ('bower_components/ckeditor/ckeditor.js',)
        css = {'all': ('css/admin-fix.css',)}


class CakeAdmin(admin.ModelAdmin):
    formfield_overrides = {models.TextField: {'widget': forms.Textarea(attrs={'class':'ckeditor'})},}

    class Media:
        js = ('bower_components/ckeditor/ckeditor.js',)
        css = {'all': ('css/admin-fix.css',)}

admin.site.register(Category, CategoryAdmin)
admin.site.register(Cake, CakeAdmin)
