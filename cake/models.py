from django.db import models
from django.conf import settings
from django.template.defaultfilters import slugify
from unidecode import unidecode
import os


class Category(models.Model):
    """Taxonomy model"""
    name = models.CharField(max_length=60, unique=True)
    description = models.TextField(blank=True, null=True)
    created_date = models.DateTimeField(auto_now_add=True)
    changed_date = models.DateTimeField(auto_now=True)
    slug = models.SlugField(editable=False)

    def save(self, *args, **kwargs):
        if not self.id:
            self.slug = slugify(unidecode(self.name))

        super(Category, self).save(*args, **kwargs)

    def __unicode__(self):
        return u'%s' % (
            self.name,
        )


class Cake(models.Model):
    """Cake model"""
    name = models.CharField(max_length=60, unique=True)
    description = models.TextField(blank=True, null=True)
    category = models.ManyToManyField(Category, related_name='category')
    price = models.PositiveIntegerField(default=0)
    on_slider = models.BooleanField(default=False)
    image = models.ImageField(upload_to='cakes', default=os.path.join(settings.STATIC_URL,'no-avatar.jpg'))
    created_date = models.DateTimeField(auto_now_add=True)
    changed_date = models.DateTimeField(auto_now=True)
    slug = models.SlugField(editable=False)

    def save(self, *args, **kwargs):
        if not self.id:
            self.slug = slugify(unidecode(self.name))

        super(Cake, self).save(*args, **kwargs)

    def __unicode__(self):
        return u'%s' % (
            self.name,
        )
