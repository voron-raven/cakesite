"""cakesite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    url(r'^$', 'cake.views.main', name='main'),
    url(r'^ajax$', 'cake.views.main', name='ajax_main'),
    url(r'^about$', 'cake.views.about_page', name='about_page'),
    url(r'^ajax/about$', 'cake.views.about_page', name='ajax_about_page'),
    url(r'^contact$', 'cake.views.contact_page', name='contact_page'),
    url(r'^ajax/contact$', 'cake.views.contact_page', name='ajax_contact_page'),
    url(r'^category/(?P<page_slug>.+)$', 'cake.views.category', name='category'),
    url(r'^ajax/category/(?P<page_slug>.+)$', 'cake.views.category', name='ajax_category'),
    url(r'^cake/(?P<cake_slug>.+)$', 'cake.views.cake', name='cake'),
    url(r'^ajax/cake/(?P<cake_slug>.+)$', 'cake.views.cake', name='ajax_cake'),

    url(r'^admin/', include(admin.site.urls)),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
